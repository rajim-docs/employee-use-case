package com.academia;

/**
 * @author rajim 2022-05-07.
 * @project IntelliJ IDEA
 */
public class BasicPlusCommissionEmployee extends CommissionEmployee {

    private double baseSalary;

    public double getBaseSalary() {
        return baseSalary;
    }

    public void setBaseSalary(double baseSalary) {
        this.baseSalary = baseSalary;
    }
}
