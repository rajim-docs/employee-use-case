package com.academia;

import java.util.Scanner;

/**
 * @author rajim 2022-05-07.
 * @project IntelliJ IDEA
 */
public class HourlyEmployee extends Employee {

    private double hourlyWage;

    private int totalHours;

    public double getHourlyWage() {
        return hourlyWage;
    }

    public void setHourlyWage(double hourlyWage) {
        this.hourlyWage = hourlyWage;
    }

    public int getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(int totalHours) {
        this.totalHours = totalHours;
    }

    public void readAndSaveData() {
        Scanner scanner = new Scanner(System.in);
        // Read employee
        // Save into db
    }
}
