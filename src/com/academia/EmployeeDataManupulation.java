package com.academia;

import java.util.Scanner;

/**
 * @author rajim 2022-05-07.
 * @project IntelliJ IDEA
 */
public class EmployeeDataManupulation {

    public void readEmployeeDetail() {

        int option = 0;
        while(option != 3) {
            Scanner scanner = new Scanner(System.in);

            System.out.println("Enter 1, To Read Hourly Employee");
            System.out.println("Enter 2, To Read Salary Employee");
            // ADD other
            System.out.println("Enter 3, To exit from Read Menu.");
            option = scanner.nextInt();

            switch (option) {
                case 1:
                    // read hourly employee
                    HourlyEmployee hourlyEmployee = new HourlyEmployee();
                    hourlyEmployee.readAndSaveData();
                    break;
                case 2:
                    // read salary employee
                    break;
                default:
                    System.out.println("Please, enter valid input.");
            }


        }
    }
}
