package com.academia;

/**
 * @author rajim 2022-05-07.
 * @project IntelliJ IDEA
 */
public class SalaryEmployee extends Employee {

    private double monthlySalary;

    public double getMonthlySalary() {
        return monthlySalary;
    }

    public void setMonthlySalary(double monthlySalary) {
        this.monthlySalary = monthlySalary;
    }
}
