package com.academia;

/**
 * @author rajim 2022-05-07.
 * @project IntelliJ IDEA
 */
public class CommissionEmployee extends Employee {

    private double grossSales;

    private double commissionRate;

    public double getGrossSales() {
        return grossSales;
    }

    public void setGrossSales(double grossSales) {
        this.grossSales = grossSales;
    }

    public double getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(double commissionRate) {
        this.commissionRate = commissionRate;
    }
}
